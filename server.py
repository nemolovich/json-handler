#! /usr/bin/python
# -*- coding:utf-8 -*-
# Author: Brian GOHIER
# 
from flask import Flask, request, jsonify, json
from flask_cors import CORS
import logging, os, time, shutil
from logging.handlers import TimedRotatingFileHandler

POST_KEY = 'POST_KEY'
GET_KEY = 'GET_KEY'

APP_ROUTE = '/'
TIME_ZONE = os.getenv('TIME_ZONE', 'Europe/Paris')
os.environ['TZ'] = TIME_ZONE
APP_PORT = int(os.getenv('APP_PORT', '4999'))
SRV_PATH = os.getenv('SRV_PATH', '/srv/python')
DATA_PATH = SRV_PATH + '/data'
DATA_FILE = DATA_PATH + '/data_%s.json'
CONFIG_PATH = SRV_PATH + '/config'
CONFIG_FILE = CONFIG_PATH + '/config.json'
JSON_MIME = 'application/json'
LOG_FILE = SRV_PATH + '/logs/JSON_HANDLER.log'

INIT_FILE = SRV_PATH + '/config.init.json'

if os.path.isfile(INIT_FILE) and not os.path.isfile(CONFIG_FILE):
	print 'Copy init file "%s" to "%s"' % (INIT_FILE, CONFIG_FILE)
	shutil.move(INIT_FILE, CONFIG_FILE)

SECRET_KEY = None
DATA_KEYS = {}

app = Flask(__name__)
CORS(app)
app.secret_key = POST_KEY

LOG_FORMAT = '%(levelname)-6.6s[%(asctime)s]: %(message)s'
handler = TimedRotatingFileHandler(LOG_FILE, when='midnight', interval=1)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(LOG_FORMAT, '%d/%m/%Y %H:%M:%S')
handler.setFormatter(formatter)
app.logger.addHandler(handler)
app.logger.setLevel(logging.DEBUG)

def logDebug(msg):
	# app.logger.debug('DEBUG [%s]: %s' % (str(time.strftime("%d/%m/%Y %H:%M:%S", time.localtime(int(time.time())))), msg))
	app.logger.debug(msg)

def logInfo(msg):
	# app.logger.info('INFO  [%s]: %s' % (str(time.strftime("%d/%m/%Y %H:%M:%S", time.localtime(int(time.time())))), msg))
	app.logger.info(msg)

def logWarning(msg):
	# app.logger.warning('WARN  [%s]: %s' % (str(time.strftime("%d/%m/%Y %H:%M:%S", time.localtime(int(time.time())))), msg))
	app.logger.warning(msg)

def logError(msg, err = None):
	# app.logger.error('ERROR [%s]: %s' % (str(time.strftime("%d/%m/%Y %H:%M:%S", time.localtime(int(time.time())))), msg))
	app.logger.error(msg)
	if err != None:
		app.logger.exception(err)

app.logger.info('===================================')
logInfo('Starting application')

if os.path.isfile(CONFIG_FILE):
	f = open(CONFIG_FILE, 'r')
	conf = f.read()
	f.close()
	try:
		j = json.loads(conf)
		if 'api_token' in j:
			SECRET_KEY = j['api_token']
			logInfo('Token: %s' % SECRET_KEY)
		if 'api_route' in j:
			APP_ROUTE = j['api_route']
			logInfo('Route: %s' % APP_ROUTE)
		if 'applications' in j:
			DATA_KEYS = {app['app_name'].upper():{GET_KEY:app['get'], POST_KEY:app['post']}for app in j['applications']}
			logInfo('Apps: %s' % DATA_KEYS)
		logDebug('Configuration file "%s" loaded' % CONFIG_FILE)
	except (ValueError, KeyError) as e:
		logError('Cannot parse JSON from "%s"' % CONFIG_FILE, e)
else:
	logError('Configuration file "%s" not found' % CONFIG_FILE)

API_KEY_ATTR = 'api_key'
APP_NAME_ATTR = 'app'
APP_KEY_ATTR = 'key'

ATTRS = [API_KEY_ATTR, APP_NAME_ATTR, APP_KEY_ATTR]

@app.route(APP_ROUTE, methods=['GET', 'POST'])
def process():
	
	httpCode = 500
	httpMsg = httpContent = json.dumps('Server error')
	
	requestArgs = request.args
	requestMeth = request.method
	
	if set(ATTRS) & set(requestArgs) != set(ATTRS):
		httpMsg = httpContent = json.dumps('Invalid attributes')
		httpCode = 400
	else:
		secretKey = requestArgs.get(API_KEY_ATTR)
		appName = requestArgs.get(APP_NAME_ATTR).upper()
		appExists = appName in DATA_KEYS.keys()
		getKey = DATA_KEYS[appName][GET_KEY] if appExists else None
		postKey = DATA_KEYS[appName][POST_KEY] if appExists else None
		key = requestArgs.get(APP_KEY_ATTR)
		dataFile = DATA_FILE % appName
		
		if secretKey != SECRET_KEY:
			httpMsg = httpContent = json.dumps('Access denied')
			httpCode = 403
		elif (requestMeth == 'GET' and key != getKey != None) or \
			(requestMeth == 'POST' and key != postKey != None):
			httpMsg = httpContent = json.dumps('Unauthorized')
			httpCode = 401
		elif not appExists:
			httpMsg = httpContent = json.dumps('Unknown application \'%s\'' % appName)
			httpCode = 400
		elif requestMeth == 'POST' and key == DATA_KEYS[appName][POST_KEY]:
			content = request.get_json(silent = True)
			str_content = json.dumps(content)
			if str_content == 'null':
				httpMsg = httpContent = json.dumps('Cannot read JSON content')
				httpCode = 400
			else:
				f = open(dataFile, 'w')
				content['update_date'] = str(time.strftime("%d/%m/%Y %H:%M:%S", time.localtime(int(time.time()))))
				f.write(json.dumps(content))
				f.close()
				httpMsg = httpContent = json.dumps('Document stored successfully')
				httpCode = 201
		elif requestMeth == 'GET' and key == DATA_KEYS[appName][GET_KEY]:
			if not os.path.isfile(dataFile):
				httpMsg = httpContent = json.dumps('There is no result')
				httpContent = httpMsg
			else:
				f = open(dataFile, 'r')
				content = f.read()
				f.close()
				httpMsg = 'Content returned successfully'
				httpContent = content
				httpCode = 200
	
	logInfo('[%d]: %s' % (httpCode, httpMsg))
	result = app.response_class(
		response = httpContent,
		status = httpCode,
		mimetype = JSON_MIME
	)
	return result

if __name__ == '__main__':
	app.run(host = '0.0.0.0', port = APP_PORT, debug = True)


