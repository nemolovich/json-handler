# JSON-HANDLER

This image can be used as bridge between servers to transfer JSON data.

## Build
```sh
docker build -t capgemininantes/jsonhandler ./
```

## Run

```sh
docker run -p 5000:4999 docker -e TIME_ZONE="Europe/Paris" \
    -v /opt/volumes/jsonhandler/data:/srv/python/data:Z \
    -v /opt/volumes/jsonhandler/logs:/srv/python/logs:Z \
    -v /opt/volumes/jsonhandler/config:/srv/python/config:Z \
    --name jsonhandler capgemininantes/jsonhandler
```

### --volume|-v

| **Container path**   | **Description**                                     |
|:---------------------|:----------------------------------------------------|
| `/srv/python/config` | Contains the configuration file `config.json` as JSON file |
| `/srv/python/logs`   | Contains server logs                                |
| `/srv/python/data`   | Contains data stored for applications as JSON files |

### --env|-e

| **Variable name** | **Default value** | **Description**                           |
|:------------------|:------------------|-------------------------------------------|
| `TIME_ZONE`       | "UTC"             | Application TimeZone (eg. 'Europe/Paris') |

## Configuration file
The configuration file is placed as `/srv/python/config/config.json` inside the container.

It must be structured as:
```JSON
{
	"api_token":	<API_TOKEN>,
	"api_route":	<API_ROUTE>,
	"applications":	[
		{
			"app_name":	<APP_NAME>,
			"post":		<APP_POST_KEY>,
			"get":		<APP_GET_KEY>
		}
	]
}
```

The **applications** key contains an array of elements with same structure as describe above.
For every application configured there is a data file stored as JSON into `/srv/python/data`
folder inside the container.

The configuration JSON values:

| Value            | Description                               |
|:-----------------|:------------------------------------------|
| `<API_TOKEN>`    | The token to access on application        |
| `<API_ROUTE>`    | The application context path              |
| `<APP_NAME>`     | The name of the application to store data |
| `<APP_POST_KEY>` | The token for POST request for this application |
| `<APP_GET_KEY>`  | The token for GET request for this application |

