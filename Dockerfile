FROM centos:7
MAINTAINER bgohier

RUN yum update -y
RUN yum install -y python-devel

ENV SRV_PATH=/srv/python
ENV TIME_ZONE=UTC
WORKDIR ${SRV_PATH}

ADD https://gitlab.com/nemolovich/shellscripts/raw/master/commons/rand-keygen /usr/bin/rand-keygen
RUN chmod +x /usr/bin/rand-keygen && rand-keygen ; exit $?

RUN curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py" && python get-pip.py \
  && rm get-pip.py && pip -V ; exit $?

COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

RUN mkdir -p ${SRV_PATH}/{config,logs,data}

VOLUME ${SRV_PATH}/config
VOLUME ${SRV_PATH}/data
VOLUME ${SRV_PATH}/logs

COPY config.json ${SRV_PATH}/config.init.json
COPY server.py ${SRV_PATH}/server.py

RUN sed -i 's/\("api_token":\s*\)"\(\w\+\)"/\1"'$(rand-keygen)'"/' ${SRV_PATH}/config.init.json
RUN cat ${SRV_PATH}/config.init.json


ENTRYPOINT ["python"]
CMD ["server.py"]
